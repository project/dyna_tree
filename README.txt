Dyna Tree

By using Dyna Tree JS library Dyna Tree module provides Tree view of Taxonomies.
Very useful in managing Taxonomies if you have large number of Terms in a 
Vocabulary.

About Dynatree :
Dynatree is a dynamic JavaScript tree view control with support for checkboxes,
drag'n'drop, and lazy loading.

Installation :
Download the Dynatree library from http://wwwendt.de/tech/dynatree/index.html and place it in libraries/ folder.
Install the module as normal Drupal module.
