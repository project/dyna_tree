(function ( $, Drupal, drupalSettings ) {
    Drupal.behaviors.dyna_tree = {
        attach: function(context, settings) {
        	var arr_dyna = drupalSettings.initDynatree;
        	for( i in arr_dyna ) {
	        	$("#" + arr_dyna[i]).dynatree({
	                checkbox: true,
	                clickFolderMode: 3,
	                selectMode: 2,
	                title: "Dyna Tree",
	                fx: {height: "toggle", duration: 200},
	                autoFocus: false,
	                initAjax: {
	                    url: drupalSettings.path.baseUrl + "dyna-tree/ajax/" + arr_dyna[i]
	                },
	                onPostInit: function (isReloading, isError) {
	
	                },
	                onFocus: function (node) {
	                    var options = $("#selected_item").chosen().val();
	                    $.each(options, function (index, item) {
	                        $("#" + arr_dyna[i]).dynatree("getTree").getNodeByKey(item).select();
	                    });
	                },
	                onExpand: function (node) {
	                    var options = $("#selected_item").chosen().val();
	                    $.each(options, function (index, item) {
	                        $("#" + arr_dyna[i]).dynatree("getTree").getNodeByKey(item).select();
	                    });
	                },
	                onActivate: function (node) {
	                    $("#echoActive").text("" + node + " (" + node.getKeyPath() + ")");
	                },
	                onLazyRead: function (node) {
	                    node.appendAjax({
	                        url: drupalSettings.path.baseUrl + "dyna-tree/ajax/child/" + node.data.key,
	                        success: function(data) {
	                            if (node.data.checkboxClicked === true) {
	                                selectNodeChild(node, true);
	                            }
	                        }
	                    });
	                },
	                onClick: function(node, event) {
	                    var clickedTarget = node.getEventTargetType(event);
	                    if(clickedTarget === "title" && node.data.isFolder){
	                        if (node.childList === null) {
	                            selectNodeChild(node, true);
	                        }
	                        else {
	                            var child_selected = false;
	                            for (var i = 0; i < node.childList.length; i++) {
	                                var childNode = node.childList[i];
	                                if (childNode.isSelected()) {
	                                    child_selected = true;
	                                    break;
	                                }
	                            }
	                            selectNodeChild(node, !child_selected);
	                        }
	                        return false;
	                    }
	                    else if(clickedTarget === "checkbox" && node.data.isFolder){
	                        var level = node.getLevel();
	                        if (level == 2) {
	                            selectNodeChild(node, !node.isSelected());
	                        }
	                    }
	                },
	                onSelect: function (select, node) {
	                    // Display list of selected nodes
	                    var selNodes = node.tree.getSelectedNodes();
	                    var aux = 0;
	                    // convert to title/key array
	                    var selKeys = $.map(selNodes, function (node) {
	                        var index = selNodes.length;
	                        return '"' + aux++ + '":{"tid":' + node.data.key + '}';
	                    });
	                    aux = 0;
	                    $('#selected').text('{' + selKeys.join(",") + '}');
	                    var myvalue = node.data.key;
	                    //Make the checked option selected in chosen widget
	                    if (select) {
	                        $('#selected_item option[value="' + myvalue + '"]').attr('selected', 'selected');
	                        $('#selected_item').chosen().change();
	                        $('#selected_item').trigger('chosen:updated');
	                    }
	                    else {
	                        $('#selected_item option[value="' + myvalue + '"]').removeAttr('selected');
	                        $('#selected_item').chosen().change();
	                        $('#selected_item').trigger('chosen:updated');
	                    }
	                }
	            });
	            $("#btnReloadActive").click(function () {
	                var node = $("#" + arr_dyna[i]).dynatree("getActiveNode");
	                if (node && node.isLazy()) {
	                	node.reloadChildren(function (node, isOk) {
	                    });
	                } else {
	                    alert("Please activate a lazy node first.");
	                }
	            });
        	}
        	
        	/**
             * Select/Unselect all the childs of the given node in dynatree.
             *
             * @param {object} node
             * @param {bool} op  (select => true | unselect => false)
             */
            function selectNodeChild(node, op) {
                // This JS setting is set in module so that it will work only for
                // location dynatree element (not on others).
                //var select_all_child = Drupal.settings.ge_bfl_roles_dynatree.child_select_all;

                // If select all child settings is set for field.
                var select_all_child = true;
                if (select_all_child) {

                    // No don't need to do anything for the root('All') node.
                    var level = node.getLevel();
                    if (level == 1) {
                        return;
                    }

                    // If node is not expanded, just expand it.
                    var expanded = node.isExpanded();
                    if (!expanded) {
                        // Using settimeout because of race condition.
                        setTimeout(function(){
                            node.expand();
                        }, 1);
                    }

                    // Creating and initializing this variable so that in expand callback,
                    // we able to select/de-select child of the given parent.
                    node.data.checkboxClicked = true;

                    // Select all the children of the given node.
                    if (node.childList !== null) {
                        for (var i = 0; i < node.childList.length; i++) {
                                var childNode = node.childList[i];
                                childNode.select(op);
                        }
                    }
                }
            }
            
            if( undefined != drupalSettings.arr_dynatree_items && '' != drupalSettings.arr_dynatree_items ) {
            	$('#' + drupalSettings.arr_dynatree_items.field_id ).val( drupalSettings.arr_dynatree_items.dynatree_selected_items.join(',') );
            	drupalSettings.arr_dynatree_items = ''
            }

      }
    };
}(jQuery, Drupal, drupalSettings));