<?php

namespace Drupal\dyna_tree\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class DynatreeController extends ControllerBase {

    public function openDynatreeDialog( $field_id, $vocabulary ) {

        $response = new AjaxResponse();

        $arr_target = explode( ',' , $vocabulary );

        $form = \Drupal::formBuilder()->getForm('\Drupal\dyna_tree\Form\DynatreeForm', $field_id, $vocabulary );
        
        $arr_obj_voc = taxonomy_vocabulary_load_multiple($arr_target);

        $arr_voc_names = [];
        foreach( $arr_target as $vid ) {
            $arr_voc_names[] = $arr_obj_voc[$vid]->get('name');
        }

        $str_label = implode( ' ,', $arr_voc_names );
        
        $response->addCommand( new OpenModalDialogCommand( $str_label, $form, ['width' => '800'] ) );

        return $response;
    }

    public function ajaxVocabulary( $vocabulary ) {

        $form = array();

        $form['#attached']['library'][] = 'dyna_tree/dyna_tree';

        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vocabulary,0,2);

        $arr_init_dyna = array();
        foreach( $terms as $term ) {
            if( $term->parents[0] == 0 ) {
                $arr_init_dyna[$term->tid] = $term->name;
            }
        }
        $init_dyna = array();
        foreach( $terms as $term ) {
            foreach( $arr_init_dyna as $tid => $dyna ) {
                $init_dyna[$tid]['name'] = $dyna;
                if( $term->parents[0] == $tid ) {
                    $init_dyna[$tid]['isFolder'] = true;
                } else if( $term->parents[0] == 0 && ( isset( $init_dyna[$tid]['isFolder'] ) && $init_dyna[$tid]['isFolder'] != true ) ) {
                    $init_dyna[$tid]['isFolder'] = false;
                }
            }
        }
        $str_init_dyna = array();

        foreach( $init_dyna as $tid => $dyna ) {
            $bool = isset( $dyna['isFolder'] ) && true == $dyna['isFolder'] ? 'true' : 'false';
            $str_init_dyna[] = '{"title": "' . $dyna['name'] . '", "isFolder": ' . $bool . ', "key": "' . $tid . '", "unselectable":false, "hideCheckbox":false, "target":"_self", "href":"", "select":false, "expand":false, "isLazy":true, "children":[]}';
        }
        $str_init_dyna = '[' . implode( ',' , $str_init_dyna ) . ']';

        $response = new Response();
        $response->setContent( $str_init_dyna );
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function ajaxChildTerms( $tid ) {

        $json = '';
        $islazy = true;

        $childs = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren( $tid );

        foreach ( $childs as $child ) {
            $hasChilds = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren( $child->tid->value );
            if ( true == count( $hasChilds ) > 0 ) {
                $true = 'true';
                $json .= '{"key":"' . $child->tid->value  . '","title":"' . $child->name->value . '","target":"_self", "isLazy":"'.$islazy.'", "isFolder" :' . $true . ',"select":"true"},';
            }
            else {
                $json .= '{"key":"' . $child->tid->value  . '","title":"' . $child->name->value . '", "target":"_self", "select":"true"},';
            }
        }

        $json = '[' . substr_replace($json, "", -1) . ']';

        $response = new Response();
        $response->setContent( $json );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
        
    }
}