<?php

namespace Drupal\dyna_tree\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\SettingsCommand;

class DynatreeForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'dynatree_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $field_id = [], $vocabulary = []) {

        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

        $arr_target = explode(',', $vocabulary);

        $options = [];
        $arr_terms = [];
        foreach ($arr_target as $vid) {
            $arr_terms[] = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
        }

        foreach ($arr_terms as $terms) {
            foreach ($terms as $term) {
                $options[$term->tid] = $term->name;
            }
        }

        $arr_obj_voc = taxonomy_vocabulary_load_multiple($arr_target);

        $arr_voc_names = [];
        foreach ($arr_target as $vid) {
            $arr_voc_names[] = $arr_obj_voc[$vid]->get('name');
        }

        $str_label = implode(' ,', $arr_voc_names);

        $form['selected_item'] = array(
          '#name' => 'selected_item[]',
          '#type' => 'select',
          '#title' => t('Selected ' . $str_label),
          '#options' => $options,
          '#attributes' => array(
            'class' => array('chosen-widget'),
            'multiple' => TRUE,
            'id' => 'selected_item'
          ),
          '#weight' => -90,
        );

        foreach ($arr_target as $vid) {
            $form['dynatree'][$vid] = array(
              '#type' => 'item',
              '#title' => t('From'),
              '#title_display' => 'invisible',
              '#markup' => '<div id="' . $vid . '"></div>',
              '#weight' => 95,
            );
        }

        $form['field_id'] = array(
          '#name' => 'field_id',
          '#type' => 'hidden',
          '#weight' => 80,
          '#value' => $field_id,
        );

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Submit'),
          '#weight' => 100,
          '#ajax' => [
            'callback' => [$this, 'submitForm'],
            'event' => 'click',
          ],
          '#attributes' => [
            'class' => [
              'use-ajax',
            ],
          ],
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $command = new CloseModalDialogCommand();
        $response = new AjaxResponse();
//         $form_state->getValue('selected_item')
        $arr_term_objs = taxonomy_term_load_multiple($form_state->getValue('selected_item'));

//         print_r( $term_obj );die;
        $arr_tids_with_names = [];

        foreach ($arr_term_objs as $key => $term_obj) {
            $arr_tids_with_names[] = $term_obj->getName() . ' (' . $key . ')';
        }

        $response->addCommand(new SettingsCommand(['arr_dynatree_items' => ['field_id' => $form_state->getValue('field_id'), 'dynatree_selected_items' => $arr_tids_with_names]], TRUE));
        $response->addCommand($command);
        return $response;
    }

}
