<?php

namespace Drupal\dyna_tree\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * A dynatree widget.
 *
 * @FieldWidget(
 *   id = "dynatree",
 *   label = @Translation("Dynatree widget"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class DynatreeWidget extends EntityReferenceAutocompleteWidget {

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

        $arr_element = parent::formElement($items, $delta, $element, $form, $form_state);

        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
        $form['#attached']['library'][] = 'dyna_tree/dyna_tree';
        
        $arr_target = $arr_element['target_id']['#selection_settings']['target_bundles'];
        
        foreach( $arr_target as $vid ) {
            $form['#attached']['drupalSettings']['initDynatree'][] = $vid;
        }
        
        $path = base_path() . 'dyna-tree/search';
        
        $str_target = implode( ',', $arr_target);
        
        $path .= '/' . 'edit-' . str_replace('_', '-', $items->getName() ) . '-target-id/' . $str_target; 

        $arr_element['target_id']['#id'] = 'edit-' . str_replace('_', '-', $items->getName() ) . '-target-id';
        $arr_element['target_id']['#tags'] = TRUE;
        $arr_element['target_id']['#default_value'] = $items->referencedEntities();
        $arr_element['target_id']['#prefix'] = '<div class="dynatree-wrapper">';
        $arr_element['target_id']['#suffix'] = '<div class="dynatree-inner-wrapper"><a class="use-ajax" href="' . $path . '">Search</a></div></div>';

        return $arr_element;
    }
    
    /**
     * {@inheritdoc}
     */
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
        return $values['target_id'];
    }
    
}